const app = new PIXI.Application({
    "width": 1000,
    "height": 800,
    "view": document.getElementById("game_canvas"),
    "backgroundColor": 0x000000,
    "autoStart": true
});

var graphics = new PIXI.Graphics();

//Number 1
graphics.lineStyle(0,0x000000);
graphics.beginFill(0x0000FF);
graphics.drawCircle(100,100, 45);
graphics.endFill();

//Number 2
graphics.lineStyle(0,0x000000);
graphics.beginFill(0xFF0000);
graphics.drawRect(50,200,150, 210)
graphics.endFill();

//Number 3
graphics.lineStyle(0,0x000000);
graphics.beginFill(0x008000);
graphics.drawEllipse(200,600, 180, 75);
graphics.endFill();

//Number 4
graphics.lineStyle(2,0x0000FF);
graphics.drawCircle(300,100, 45);

//Number 5
graphics.lineStyle(2,0xFF0000);
graphics.drawRect(300,200,150, 210)

//Number 6
graphics.lineStyle(2,0x008000);
graphics.drawEllipse(600,600, 180, 75);

//Number 7
graphics.lineStyle(4,0xFFC0CB);
graphics.moveTo(100, 700);
graphics.lineTo(100, 750);
graphics.moveTo(100, 750);
graphics.lineTo(150, 750);
graphics.moveTo(150, 750);
graphics.lineTo(150, 700);
graphics.moveTo(150, 700);
graphics.lineTo(100, 700);

//Number 8
graphics.lineStyle(4,0xFFC0CB);
graphics.moveTo(240, 700);
graphics.lineTo(220, 750);
graphics.moveTo(220, 750);
graphics.lineTo(260, 720);
graphics.moveTo(260, 720);
graphics.lineTo(210, 720);
graphics.moveTo(210, 720);
graphics.lineTo(250, 750);
graphics.moveTo(250, 750);
graphics.lineTo(240, 700);

//Number 9
graphics.lineStyle(0,0x000000);
graphics.beginFill(0x008000);
graphics.drawEllipse(610,110, 20, 60);
graphics.endFill();

graphics.lineStyle(0,0x000000);
graphics.beginFill(0x008000);
graphics.drawEllipse(690,110, 20, 60);
graphics.endFill();

graphics.lineStyle(0,0x000000);
graphics.beginFill(0xFFFF00);
graphics.drawCircle(650,170, 60);
graphics.endFill();

graphics.lineStyle(0,0x000000);
graphics.beginFill(0xFF0000);
graphics.drawCircle(620,160, 5);
graphics.endFill();

graphics.lineStyle(0,0x000000);
graphics.beginFill(0xFF0000);
graphics.drawCircle(680,160, 5);
graphics.endFill();

graphics.lineStyle(4,0x0000FF);
graphics.moveTo(610, 185);
graphics.lineTo(690, 185);

graphics.lineStyle(3,0x0000FF);
graphics.beginFill(0xFFFFFF);
graphics.drawRect(630,185, 15, 22)
graphics.endFill();

graphics.lineStyle(3,0x0000FF);
graphics.beginFill(0xFFFFFF);
graphics.drawRect(655,185, 15, 22)
graphics.endFill();

app.stage.addChild(graphics);